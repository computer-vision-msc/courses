from model import Model
from problem import Problem
from state import State
from cardinal import *
from tree import TreeNode

UNIFORME_COST = 0
A_START_1 = 1
A_START_2 = 2

# Funções utilitárias
def printExplored(explored):
    """Imprime os nós explorados pela busca.
    @param explored: lista com os nós explorados."""

    print("--- Explorados --- (TAM: {})".format(len(explored)))
    stateStr = ""
    for states in explored:
        for state in states:
            stateStr += str(state)
    print("[{0}]".format(stateStr))
    print("\n")

def printFrontier(frontier):
    """Imprime a fronteira gerada pela busca.
    @param frontier: lista com os nós da fronteira."""

    print("--- Fronteira --- (TAM: {})".format(len(frontier)))
    for node in frontier:
        print(node,end=' ')
        print("\n")

def buildPlan(solutions):

    solutionNode = solutions[0]
    solucao = 1
    nSolution = len(solutions)
    if nSolution > 1:
        for i in range(1,nSolution):
            if solutions[i].getFn() < solutionNode.getFn():
                solutionNode = solutions[i]
                solucao = i+1

    print("!!! Custo:        {}".format(solutionNode.gn))
    print("!!! Profundidade: {}\n".format(solutionNode.depth))

    depth = solutionNode.depth
    solution = [0 for i in range(depth)]
    parent = solutionNode
    print("------------SOLUCAO---------")
    print(solucao)
    for i in range(len(solution) - 1, -1, -1):
        solution[i] = parent.action
        parent = parent.parent
    return solution


class Agent:
    """"""
    counter = -1 # Contador de passos no plano, usado na deliberação

    def __init__(self, model):
        """Construtor do agente.
        @param model: Referência do ambiente onde o agente atuará."""
        self.model = model

        self.prob = Problem()
        self.prob.createMaze(8, 8)
        self.prob.mazeBelief.putVerticalWall(0,7,0)
        self.prob.mazeBelief.putVerticalWall(3,5,4)
        self.prob.mazeBelief.putVerticalWall(0,7,7)
        # self.prob.mazeBelief.putVerticalWall(5,5,2)
        # self.prob.mazeBelief.putVerticalWall(8,8,2)

        self.prob.mazeBelief.putHorizontalWall(1,6,0)
        self.prob.mazeBelief.putHorizontalWall(1,6,7)
        self.prob.mazeBelief.putHorizontalWall(1,2,3)
        # self.prob.mazeBelief.putHorizontalWall(3,5,3)
        # self.prob.mazeBelief.putHorizontalWall(7,7,3)
        #
        # self.prob.mazeBelief.putVerticalWall(6,7,4)
        # self.prob.mazeBelief.putVerticalWall(5,6,5)
        # self.prob.mazeBelief.putVerticalWall(5,7,7)

        # Posiciona fisicamente o agente no estado inicial
        initial = self.positionSensor()
        #Posiciona fisicamente as caixas no estado inicial
        #Retorna um array de states
        intialBoxes = self.boxPositionSensor()

        self.prob.defInitialState(initial.row, initial.col)
        self.prob.defIntialBoxState(intialBoxes)

        # Define o estado atual do agente = estado inicial
        self.currentState = self.prob.initialState

        # Define o estados atuais da caixa = estados iniciais das caixas
        #self.boxCurrentState = self.prob.intialBoxState

        # Define o estado atual da caixa + agente
        self.currentStates = [self.currentState]
        for i in range(1,len(self.prob.intialStates)):
            #if i > 0: #0 é o agente
            self.currentStates.append(self.prob.intialStates[i])

        # Define os estados objetivo
        goal1 = [6,1]
        goal2 = [6,2]
        goal3 = [6,3]
        goal4 = [6, 4]
        goal5 = [6, 5]
        goal6 = [6, 6]
        #goals = [goal1,goal2,goal3,goal4,goal5,goal6]
        goals = [goal1, goal2]
        self.prob.defGoalState(goals)
        self.model.setGoalPos(goals)

        # Plano de busca
        self.plan = None

    def printPlan(self,plan):
        """Apresenta o plano de busca."""    
        print("--- PLANO ---")

        for plannedAction in plan:
            print("{} > ".format(action[plannedAction]),end='')
        print("FIM\n\n")

    def deliberate(self):
        # Primeira chamada, realiza busca para elaborar um plano

        # criar loop aqui para concatenar planos
        self.goal = [];
        if self.counter == -1:
            self.plan = self.cheapestFirstSearch(2)  # 0 = custo uniforme, 1 = A* com colunas, 2 = A*
            if self.plan != None:
                self.printPlan(self.plan)
            else:
                print("SOLUÇÃO NÃO ENCONTRADA")
                return -1


        # Nas demais chamadas, executa o plano já calculado
        self.counter += 1

        # Atingiu o estado objetivo
        if self.prob.goalTest(self.currentStates):
            print("!!! ATINGIU O ESTADO OBJETIVO !!!")
            return -1
        # Algo deu errado, chegou ao final do plano sem atingir o objetivo
        if self.counter >= len(self.plan):
            print("### ERRO: plano chegou ao fim, mas objetivo não foi atingido.")
            return -1
        currentAction = self.plan[self.counter]

        print("--- Mente do Agente ---")
        print("{0:<20}{1}".format("Estado atual :",self.currentState))
        print("{0:<20}{1} de {2}. Ação= {3}\n".format("Passo do plano :",self.counter + 1,len(self.plan),action[currentAction]))
        self.executeGo(currentAction)

        # Atualiza o estado atual baseando-se apenas nas suas crenças e na função sucessora
        # Não faz leitura do sensor de posição
        self.currentStates = self.prob.suc(self.currentStates, currentAction)
        return 1

    def executeGo(self, direction):
        """Atuador: solicita ao agente física para executar a ação.
        @param direction: Direção da ação do agente
        @return 1 caso movimentação tenha sido executada corretamente."""
        self.model.go(direction)
        return 1

    def positionSensor(self):
        """Simula um sensor que realiza a leitura do posição atual no ambiente e traduz para uma instância da classe Estado.
        @return estado que representa a posição atual do agente no labirinto."""
        pos = self.model.agentPos
        return State(pos[0],pos[1])
    def boxPositionSensor(self):
        """Simula um sensor que realiza a leitura do posição atual no ambiente e traduz para uma instância da classe Estado.
                @return array de estados que representa as posições atuais das caixas no labirinto."""
        boxes = []
        for box in self.model.boxPos:
            boxes.append(State(box[0],box[1]))
        return boxes

    def hn1(self, state):
        """Implementa uma heurísitca - número 1 - para a estratégia A*.
        No caso hn1 é a distância em coluna do estado passado com argumento até o estado objetivo.
        @param state: estado para o qual se quer calcular o valor de h(n)."""

        return abs( state.col - self.prob.goalState.col )
    
    def hn2(self, state):
        """Implementa uma heurísitca - número 2 - para a estratégia A*.
        No caso hn1 é a distância distância euclidiana do estado passado com argumento até o estado objetivo.
        @param state: estado para o qual se quer calcular o valor de h(n)."""

        allDistances = [0,0]
        for i in range(0,len(self.prob.goalState)):
            for j in range(1, len(state)):
                distCol = abs(state[j].col -self.prob.goalState[i].col)
                distRow = abs(state[j].row - self.prob.goalState[i].row)
                distSquaredTemp = distCol**2 + distRow**2
                if j == 1:
                    distSquared = distSquaredTemp
                elif distSquaredTemp < distSquared:
                    distSquared = distSquaredTemp
                allDistances[i] = distSquared**0.5
        sum = 0
        for distance in allDistances:
            sum += distance
        return distance

    def cheapestFirstSearch(self, searchType):
        """Realiza busca com a estratégia de custo uniforme ou A* conforme escolha realizada na chamada.
        @param searchType: 0=custo uniforme, 1=A* com heurística hn1; 2=A* com hn2
        @param type: 0 = box, 1= agent
        @return plano encontrado"""

        # Atributos para análise de desempenho
        treeNodesCt = 0 # contador de nós gerados incluídos na árvore
        # nós inseridos na árvore, mas que não necessitariam porque o estado 
        # já foi explorado ou por já estar na fronteira
        exploredDicardedNodesCt = 0
        frontierDiscardedNodesCt = 0 

        # Algoritmo de busca
        solutions = []
        solution = None
        root = TreeNode(parent=None)
        root.state = self.prob.intialStates
        root.gn = 0
        root.hn = 0
        root.action = -1 
        treeNodesCt += 1

        # cria FRONTEIRA com estado inicial
        frontier = []
        frontier.append(root)

        # cria EXPLORADOS - inicialmente vazia
        explored = []

        print("\n*****\n***** INICIALIZAÇÃO ÁRVORE DE BUSCA\n*****\n")
        print("\n{0:<30}{1}".format("Nós na árvore: ",treeNodesCt))
        print("{0:<30}{1}".format("Descartados já na fronteira: ",frontierDiscardedNodesCt))
        print("{0:<30}{1}".format("Descartados já explorados: ",exploredDicardedNodesCt))
        print("{0:<30}{1}".format("Total de nós gerados: ",treeNodesCt + frontierDiscardedNodesCt + exploredDicardedNodesCt))

        while len(frontier): # Fronteira não vazia
            print("\n*****\n***** Início iteração\n*****\n")
            printFrontier(frontier)

            selNode = frontier.pop(0) # retira nó da fronteira
            selState = selNode.state

            print("Selecionado para expansão: {}\n".format(selNode))

            # Teste de objetivo

            achieveGoals = 0
            for goal in self.prob.goalState:
                for i in range(1,len(selState)):
                    # if i > 0:#0 é o agente
                    if selState[i] == goal:
                        achieveGoals += 1
                        if achieveGoals == len(self.prob.goalState):
                            solutions.append(selNode)
                            break



            # estados q o agente e a caixa ja passaram
            explored.append(selState)
            printExplored(explored)

            # Obtem ações possíveis para o estado selecionado para expansão

            #actions = self.prob.possibleActionsWithoutCollaterals(selState,selBoxState) # actions é do tipo [-1, -1, -1, 1, 1, -1, -1, -1]
            actions = self.prob.possibleActionsWithoutCollaterals(selState)  # actions é do tipo [-1, -1, -1, 1, 1, -1, -1, -1]

            for actionIndex, act in enumerate(actions):
                if(act < 0): # Ação não é possível
                    continue

                # INSERE NÓ FILHO NA ÁRVORE DE BUSCA - SEMPRE INSERE, DEPOIS
                # VERIFICA SE O INCLUI NA FRONTEIRA OU NÃO
                # Instancia o filho ligando-o ao nó selecionado (nSel)  
                child = selNode.addChild()

                # Obtem o estado sucessor pela execução da ação <act>
                child.state = self.prob.suc(selState, actionIndex)


                # Custo g(n): custo acumulado da raiz até o nó filho
                gnChild = selNode.gn + self.prob.getActionCost(actionIndex)
                if searchType == UNIFORME_COST:
                    child.setGnHn(gnChild, 0) # Deixa h(n) zerada porque é busca de custo uniforme
                elif searchType == A_START_1:
                    child.setGnHn(gnChild, self.hn1(child.state) )
                elif searchType == A_START_2:
                    child.setGnHn(gnChild, self.hn2(child.state) )

                child.action = actionIndex
                # INSERE NÓ FILHO NA FRONTEIRA (SE SATISFAZ CONDIÇÕES)

                # Testa se estado do nó filho foi explorado
                alreadyExplored = False


                for node in explored:
                    if(child.state == node):
                        alreadyExplored = True
                        break

                # Testa se estado do nó filho está na fronteira, caso esteja
                # guarda o nó existente em nFront
                nodeFront = None
                if not alreadyExplored:
                    for node in frontier:
                        if(child.state == node.state):
                            nodeFront = node
                            break
                # Se ainda não foi explorado
                if not alreadyExplored:
                    # e não está na fronteira, adiciona à fronteira
                    if nodeFront == None:
                        frontier.append(child)
                        frontier.sort(key=lambda x: x.getFn()) # Ordena a fronteira pelo f(n), ascendente
                        treeNodesCt += 1
                    else:
                        # Se já está na fronteira temos que ver se é melhor
                        if nodeFront.getFn() > child.getFn():       # Nó da fronteira tem custo maior que o filho
                            frontier.remove(nodeFront)              # Remove nó da fronteira (pior e deve ser substituído)
                            nodeFront.remove()                      # Retira-se da árvore
                            frontier.append(child)                  # Adiciona filho que é melhor
                            frontier.sort(key=lambda x: x.getFn())  # Ordena a fronteira pelo f(n), ascendente
                            # treeNodesCt não é incrementado por inclui o melhor e retira o pior
                        else:
                            # Conta como descartado porque o filho é pior que o nó da fronteira e foi descartado
                            frontierDiscardedNodesCt += 1
                else:
                    exploredDicardedNodesCt += 1
            
            print("\n{0:<30}{1}".format("Nós na árvore: ",treeNodesCt))
            print("{0:<30}{1}".format("Descartados já na fronteira: ",frontierDiscardedNodesCt))
            print("{0:<30}{1}".format("Descartados já explorados: ",exploredDicardedNodesCt))
            print("{0:<30}{1}".format("Total de nós gerados: ",treeNodesCt + frontierDiscardedNodesCt + exploredDicardedNodesCt))


        if(len(solutions)):
            print("!!! Solução encontrada !!!")
            print("\n{0:<30}{1}".format("Nós na árvore: ", treeNodesCt))
            print("{0:<30}{1}".format("Descartados já na fronteira: ", frontierDiscardedNodesCt))
            print("{0:<30}{1}".format("Descartados já explorados: ", exploredDicardedNodesCt))
            print("{0:<30}{1}".format("Total de nós gerados: ",treeNodesCt + frontierDiscardedNodesCt + exploredDicardedNodesCt))
            return buildPlan(solutions)
        else:
            print("### Solução NÃO encontrada ###")
            return None
