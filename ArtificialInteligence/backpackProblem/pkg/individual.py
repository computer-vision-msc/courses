import random
class Individual:
    '''Representa um individuo, nesta abordagem cada individuo sera representado por 3 arrays:
    Array genes (0 não contém o gene e 1 contém), Array Valores (valor de cada gene para mochila), Array Peso (peso de cada gene na mochila)'''
    def __init__(self,value,weight,maxWeight):
        if len(value) == len(weight):
            self.value = value
            self.weight = weight
            self.maxWeight = maxWeight
            self.size = len(value)
            self.chromosome = []

            for i in range(0,self.size):
                self.chromosome.append(random.randint(0,1))

    def getFitness(self):
        fitness = 0
        weight = 0

        for i in range(0,self.size):
            if self.chromosome[i] == 1:
                fitness += self.value[i]
                weight += self.weight[i]
        """Por padrão todo indivíduo infactviel é penalizado ( fitness = 0)
        além disso existe um flag que sinaliza se o individuo é infactivel, no caso de existencia de reparação ele pode ser facilmente localizado"""
        if weight>self.maxWeight:
            fitness = 0
        return  fitness
    def getFitting(self):
        if self.getFitness() == 0:
            return 0
        else:
            return 1/self.getFitness()

    def getWeight(self):
        weight = 0
        for i in range(0,self.size):
            if self.chromosome[i] == 1:
                weight += self.weight[i]
        return  weight

    def getNumItems(self):
        items = 0
        for i in range(0,self.size):
            if self.chromosome[i] == 1:
                items += 1
        return  items
    def infactible(self):
        if self.getWeight() > self.maxWeight:
            return True
        else:
            return False


    def __str__(self):
        return "N° Items: {0:d}, Weight: {1:d}, Fitness: {2:d}, Chromosome: {3:s}".format(self.getNumItems(),self.getWeight(),self.getFitness(),str(self.chromosome))