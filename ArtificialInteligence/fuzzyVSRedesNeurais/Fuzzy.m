clear
clc
DadosTreinamento = csvread("C:\MESTRADO\Codes\visaocomputacional\ArtificialInteligence\fuzzyVSRedesNeurais\train_normalized_matlab.csv");
size(DadosTreinamento);
EntradaTreinamento = DadosTreinamento(:,1:4); 
SaidaDesejadaTreinamento= DadosTreinamento(:,5); 
DadosTeste = csvread("C:\MESTRADO\Codes\visaocomputacional\ArtificialInteligence\fuzzyVSRedesNeurais\test_normalized_matlab.csv");
size(DadosTeste);
EntradaTeste = DadosTeste(:,1:4); 
SaidaDesejadaTeste= DadosTeste(:,5); 
wangMendel  = readfis("C:\MESTRADO\Codes\visaocomputacional\ArtificialInteligence\fuzzyVSRedesNeurais\wangMendel.fis");

saidaCalculada = evalfis(EntradaTeste,wangMendel) ;

saidaCalculada = round(saidaCalculada);

result = saidaCalculada == SaidaDesejadaTeste;
acurracy_result = sum(result)/size(result,1)
 