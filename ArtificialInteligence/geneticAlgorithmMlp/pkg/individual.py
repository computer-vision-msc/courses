import random
import pandas as pd 
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import accuracy_score
import warnings
warnings.filterwarnings('ignore')
class Individual:
    '''
    Representa um individuo, nesta abordagem cada individuo sera representado por 3 arrays, peso minimo e caminho do CSV:
    Array label (contém todas as fetures possíveis para o CSV), Array weight (peso de cada feature),
    chromosome (array booleano, se contém ou não a feature da posição correspondente)
    '''
    def __init__(self,label,weight,minWeight,csvPath):   
        self.label = label
        self.weight = weight
        self.minWeight = minWeight        
        self.chromosome = []
        self.length = len(self.label)
 
        for i in range(0, self.length):
            self.chromosome.append(random.randint(0,self.size[i]))

        self.df = pd.read_csv(csvPath)
    def getFitness(self):
        features = []
        if self.infactible():
            return 0
        else
            #Montando array para seleção de features
            features = self.getFeatures()           
           
            #Separando features
            X = self.df[features]
            y = self.fd['cls']
            #Separando dados de treino e teste
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
            #Criando MLP
            mlp = MLPClassifier(hidden_layer_sizes=(100,),solver="adam",max_iter=500,activation="logistic",learning_rate="adaptive",learning_rate_init=0.001)
            #Treinando
            mlp.fit(X_train,y_train)
            #Predizendo
            y_predicted = mlp.predict(X_test)
            return accuracy_score(y_test, y_predicted)
        """Por padrão todo indivíduo infactviel é penalizado ( fitness = 0)
        além disso existe um flag que sinaliza se o individuo é infactivel, no caso de existencia de reparação ele pode ser facilmente localizado"""        
        
    def getFeatures(self):
        features = []
         for i in range(0,self.length):
                if self.chromosome[i] == 1:
                    features.append(self.label[i])
        return features

    def getFitting(self):
        if self.getFitness() == 0:
            return 0
        else:
            return 1/self.getFitness()

    def getWeight(self):
        weight = 0
        for i in range(0,self.size):
            if self.chromosome[i] == 1:
                weight += self.weight[i]
        return  weight

    def getNumItems(self):
        items = 0
        for i in range(0,self.size):
            if self.chromosome[i] == 1:
                items += 1
        return  items
    def infactible(self):
        if self.getWeight() < self.minWeight:
            return True
        else:
            return False


    def __str__(self):

        return "N° Features: {0:d}, Fitness: {2:d}, Features: {3:s}".format(self.getNumItems(),self.getFitness(),str(self.getFeatures()))