import sys
import timeit
import cv2
import numpy as np


# cv2.Blur
# cv2.GaussianBlur
def main():
    #Parametros
    n_gaussian = 4
    n_blur =  3 #Numero de vezes que o blur será rodado para se assemelhar ao gaussiano
    sigma = 5
    kernel_size_blur = (5,5)
    kernel_size_gaussian = (9,9)
    alfa = 1.2
    beta = 0.2

    #Metodo
    boxBlur = True
    gaussian = False

    input_image = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProessing\Bloom\input-images\00-original.PNG'
    original_img = cv2.imread(input_image)
    if original_img is None:
        print ('Erro abrindo a imagem.\n')
        sys.exit()

    # Converte para float32
    original_img = original_img.astype(np.float32) / 255

    # # Mostra imagem
    # cv2.imshow('00-original', original_img)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    #Bright-pass
    mask = cv2.threshold(original_img, 0.8, 1, cv2.THRESH_BINARY)[1]

    # cv2.imshow('01-mask', mask)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    #Borrando a mascara
    for i in range (n_gaussian):
        if(gaussian):
            mask = cv2.GaussianBlur(mask,kernel_size_gaussian,sigma) + mask
        if(boxBlur):
            maskCopy = np.copy(mask)
            for j in range(n_blur):
                maskCopy = cv2.blur(maskCopy,kernel_size_blur)
            mask = mask + maskCopy



    # cv2.imshow('02-maskBlur', mask)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    # Criando imagem bloom
    img_bloom = alfa*original_img + beta *mask

    # cv2.imshow('03-Bloom', img_bloom)
    # cv2.waitKey()
    # cv2.destroyAllWindows()
    if(gaussian):
        img_name = r'\02-ImageBloomGaussian.png'
    if (boxBlur):
        img_name = r'\03-ImageBloomBoxBlur.png'
    saida = r'C:\MESTRADO\Codes\visaocomputacional\DigitalImageProcessing\Bloom\output-images'
    cv2.imwrite(saida+img_name, img_bloom * 255)

if __name__ == '__main__':
    main()