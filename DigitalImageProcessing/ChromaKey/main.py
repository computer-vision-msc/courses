import sys
import timeit
import cv2
import numpy as np
import glob


def main():
    path = r'D:\01_Pessoal\Mestrado\Codes\visaocomputacional\DigitalImageProcessing\ChromaKey\input-images\*.*'
    bg_path = r'D:\01_Pessoal\Mestrado\Codes\visaocomputacional\DigitalImageProcessing\ChromaKey\mv.png'
    bg = cv2.imread(bg_path)
    #converte para float32

    bg = bg.astype(np.float32) / 255


    for file in glob.glob(path):

        original_img = cv2.imread(file)
        if original_img is None:
            print('Erro abrindo a imagem.\n')
            sys.exit()
        # Converte para float32
        #original_img = original_img.astype(np.float32) / 255

        # Converte para HSV
        hsv = cv2.cvtColor(original_img, cv2.COLOR_BGR2HSV)



        # Mostra imagem e histograma
        # cv2.imshow('00-original', original_img)
        # cv2.waitKey()
        # cv2.destroyAllWindows()
        # Range de verde em HSV
        lower_green = np.array([50, 100, 100])
        upper_green = np.array([105, 255, 255])

        # Threshold na imagem HSV para pegar somente verde e criar uma mascara
        mask = cv2.inRange(hsv, lower_green, upper_green)
        # Mostra imagem
        # cv2.imshow('01-mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        # Converte para float32
        original_img = original_img.astype(np.float32) / 255
        mask = mask.astype(np.float32) / 255

        #Gaussian blur na mascara na tentativa de suavizar transições entre verde e não verde
        mask = cv2.GaussianBlur(mask,(5,5),0)

        # Mostra mascara suavizada
        # cv2.imshow('02-blur_mask', mask)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        # Pega o shape da imagem
        rows, cols = original_img.shape[:2]

        #Redimensiona BG
        bg_resized = cv2.resize(bg,(cols,rows))


        # Mostra bg redimensionado
        # cv2.imshow('03-bg_resized', bg_resized)
        # cv2.waitKey()
        # cv2.destroyAllWindows()

        #Loop através da mascara para criar uma imagem de saída
        output_image_channels = [np.zeros((rows,cols)),np.zeros((rows,cols)),np.zeros((rows,cols))]

        for row in range (rows):
            for col in range (cols):
                if mask[row, col] >0:
                    r = bg_resized[row, col,0]
                    g = bg_resized[row, col, 1]
                    b = bg_resized[row, col, 2]
                elif mask[row, col] == 0:
                    r = original_img[row, col, 0]
                    g = original_img[row, col, 1]
                    b = original_img[row, col, 2]
                #tentativa de suavisar transição de verde e não verde
                # else:
                #     r = 1 - mask[row, col] * original_img[row, col, 0] + mask[row, col] * bg_resized[row, col, 0]
                #     g = 1 - mask[row, col] * original_img[row, col, 1] + mask[row, col] * bg_resized[row, col, 1]
                #     b = 1 - mask[row, col] * original_img[row, col, 2] + mask[row, col] * bg_resized[row, col, 2]

                # print(r,g,b)
                output_image_channels[0][row, col] = r
                output_image_channels[1][row, col] = g
                output_image_channels[2][row, col] = b

        output_image = cv2.merge(output_image_channels)
        output_image_normalized = cv2.normalize(output_image,None,0,1,cv2.NORM_MINMAX)
        # Mostra output_image

        cv2.imshow('04-output_image', output_image_normalized)
        cv2.waitKey()
        cv2.destroyAllWindows()




if __name__ == '__main__':
    main()
